/**
 * Created by jakubmachnik on 20.09.2016.
 */
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('clean', function () {
    gulp.src('dist')
        .pipe(clean());
})


gulp.task('pack-js', function () {
    return gulp.src(['assets/js/vendor/jquery.min.js', 'assets/js/vendor/ScrollMagic.min.js',
        'assets/js/vendor/animation.velocity.min.js', 'assets/js/vendor/**/*.js', 'assets/main.js'])
        .pipe(concat('bundle.js'))
        .pipe(minify())
        .pipe(gulp.dest('assets'))
        .pipe(gulp.dest('dist'));
});

gulp.task('pack-css', function () {
    return gulp.src(['assets/css/*.css', 'assets/js/vendor/**/*.css', 'assets/main.css'])
        .pipe(concat('styles.css'))
        .pipe(autoprefixer())
        .pipe(cleanCss())
        .pipe(gulp.dest('assets'))
        .pipe(gulp.dest('dist'));
});
gulp.task('pack', ['pack-js', 'pack-css']);

gulp.task('build', function () {

    gulp.src('assets/favicon/**/*')
        .pipe(gulp.dest('dist/favicon'))

    gulp.src('assets/images/**/*')
        .pipe(gulp.dest('dist/images'))

    gulp.src('assets/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))

    gulp.src('sitemap.xml')
        .pipe(gulp.dest('dist'))
    gulp.src('assets/*.html')
        .pipe(gulp.dest('dist'))

    gulp.src('index.html')
        .pipe(gulp.dest('dist'))
    gulp.src('.htaccess')
        .pipe(gulp.dest('dist'))

});
