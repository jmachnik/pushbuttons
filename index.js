/**
 * Created by jmach on 10.06.16.
 */
/*jshint node:true*/
"use strict";


var express = require('express');
var app = express();
var path = require('path');
app.set('port', (process.env.PORT || 5000));
// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/home', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/what', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/how', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/contact', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/offer', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/contactform', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/projects', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/process', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.use(express.static('assets'));

app.listen(app.get('port'), function () {
    console.log('Example app listening on port !' + String(app.get('port')));
});