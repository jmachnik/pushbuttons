/*global ScrollMagic */
/*global TweenMax */
/*global Cubic */
/*global Bounce */
/*global Elastic */
/*global Back */
/*global History */
/*global TimelineMax */
/*global Linear */
/*global Snap */
/*gloabal mina*/
/*globals $:false */
/*globals theaterJS*/
(function () {
    "use strict";
    var strings_file = "";
    function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }

    String.prototype.endsWith = function (s) {
        return this.length >= s.length && this.substr(this.length - s.length) == s;
    }
    // var db = new PouchDB('pushbuttons');
    // var initializeDatabase = function(){
    //     var titles = {
    //         "_id": "homepageTitles",
    //         "titles": [
    //             "Oferta",
    //             "Jak pracujemy",
    //             "Projekty"
    //         ]
    //     };
    //     db.put(doc);
    //
    //     db.get('homepageTitles').then(function(doc){
    //         alert(JSON.stringify(doc.titles))
    //         }
    //     )
    // }


    var isExternalCall = false;
    var scrollMagicInitialized = false;
    var processPageOpened = false;
    var processAnimationDone = false;

    var wipeAnimation = null;
    var startVisited = false;

    var howSvg = null;
    var showOfferPage = function (offerParameter) {
        window.history.replaceState("", document.title, "offer");
        $('main').empty()
        $.get("offer.html", function (data) {
            data = '"' + data + '"';
            $("main").html(data);
            var newHTML = $('main').html();
            $('main').html(newHTML.substr(1, newHTML.length - 2));
            /*CALLBACK things */
            loadOfferPage(offerParameter);
            attachLinkMenu();
        });
        // $('main').load('offer.html',function(){
        //     loadOfferPage(offerParameter);
        //     attachLinkMenu();
        // })
    }


    var loadContactForm = function () {
        window.history.replaceState("", document.title, "contactform");
        $('main').empty()
        $('main').load('form.html', function () {
            loadContactPage();
            attachLinkMenu()
        })

    }
    var showProcessPage = function () {
        window.history.replaceState("", document.title, "process");
      $('main').empty()
        // $('#contactContainer').css('display','static')
        $('main').load('process.html', function () {
            $('.process-1-description h2')._t('lookprocessSection.maintitle1.title');
            $('.process-1-description h2').next()._t('lookprocessSection.maintitle1.description');
            $('.process-2-description h2')._t('lookprocessSection.maintitle2.title');
            $('.process-3-description h2')._t('lookprocessSection.maintitle3.title');
            $('.process-4-description h2')._t('lookprocessSection.maintitle4.title');
            $('.process-4-description p')._t('lookprocessSection.maintitle4.description');
            $('.process-5-description h2')._t('lookprocessSection.maintitle5.title');
            $('.process-5-description p')._t('lookprocessSection.maintitle5.description1');
            $('.process-5-description p').next()._t('lookprocessSection.maintitle5.description2');
            if((window.navigator.userLanguage || window.navigator.language)!='pl'){
            $(".process-svg-container").load('images/how/Proces_en.svg', function() {
                loadProcessPage();
                attachLinkMenu()

            });
            }
            else {
                $(".process-svg-container").load('images/how/Proces.svg', function () {
                    loadProcessPage();
                    attachLinkMenu()
                });
            }
        })
        // window.history.replaceState("", document.title, "process");
        // processPageOpened = true
        // loadProcessPage()
        // disableScrollMagic()
        // $('#pinContainer').css('display','none')
        // $('#processContainer').css('display','static')process
    }

    var hideProcessPage = function () {
        processPageOpened = false
        initializeHomepageScrollMagic()
        $('#pinContainer').css('display', 'block')
        $('#processContainer').css('display', 'none')
        $('#processPage > div').remove()
    }

    var attachLinkMenu = function () {
        $('#menu-home a').click(function () {
            document.location.href = '/'
        })
        $('#menu-what a').click(function () {
            document.location.href = '/what'
        })
        $('#menu-how a').click(function () {
            document.location.href = '/how'
        })
        $('#menu-projects a').click(function () {
            document.location.href = '/projects'
        })
        $('#menu-contact a').click(function () {
            document.location.href = '/contact'
        })
    }
    var attachHomePageMenu = function () {
        $('#menu-home a').click(function () {
            if (PageOpened) {
                hideProcessPage()
            }
            homepageScrollMagicController.scrollTo($('#home')[0])
        })
        $('#menu-what a').click(function () {
            if (processPageOpened) {
                hideProcessPage()
            }
            homepageScrollMagicController.scrollTo($('#what')[0])
        })
        $('#menu-how a').click(function () {
            if (processPageOpened) {
                hideProcessPage()
            }
            homepageScrollMagicController.scrollTo($('#how')[0])
        })
        $('#menu-projects a').click(function () {
            if (processPageOpened) {
                hideProcessPage()
            }
            homepageScrollMagicController.scrollTo($('#projects')[0])
        })
        $('#menu-contact a').click(function () {
            if (processPageOpened) {
                hideProcessPage()
            }
            homepageScrollMagicController.scrollTo($('#contact')[0])
        })
    }

    function linkHistoryApi(id) {
        /*jshint validthis:true */
        if (window.history && window.history.pushState && !processPageOpened) {
            if (!document.URL.endsWith(id) && !isExternalCall) {
                window.history.replaceState("", document.title, id);
            }
        }
    }

    // Init homepageScrollMagicController
    var homepageScrollMagicController = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: '100%',
            triggerHook: 'onLeave',
        }
    });

    var scrollTo = function (target) {
        var safeOffset = 1;
        TweenMax.to(window, 1, {
            scrollTo: {
                y: target + safeOffset, // scroll position of the target y + offset to trigger appropriate history event
                autoKill: false// allows user to kill scroll action smoothly

            },
            onComplete: function () {
                isExternalCall = false
            },
            ease: Linear.easeInOut
        });
    }
    var loadHomepage = function () {
        $.get("home.html", function (data) {
            data = '"' + data + '"';
            $("#homePage").html(data);
            var newHTML = $('#homePage').html();
            $('#homePage').html(newHTML.substr(1, newHTML.length - 2));

            var canvas = document.getElementById('snow'),
                ctx = canvas.getContext('2d'),
                width = ctx.canvas.width = document.body.offsetWidth,
                height = ctx.canvas.height = document.body.offsetHeight,
                animFrame = window.requestAnimationFrame ||
                    window.mozRequestAnimationFrame ||
                    window.webkitRequestAnimationFrame ||
                    window.msRequestAnimationFrame,
                snowflakes = [];

            window.onresize = function() {
                width = ctx.canvas.width = document.body.offsetWidth,
                    height = ctx.canvas.height = document.body.offsetHeight;
            }

            function update() {
                for (var i = 0; i < snowflakes.length; i++) {
                    snowflakes[i].update();
                }
            }

            function Snow() {
                this.x = random(0, width);
                this.y = random(-height, 0);
                this.radius = random(0.5, 3.0);
                this.speed = random(1, 3);
                this.wind = random(-0.5, 3.0);
            }

            Snow.prototype.draw = function() {
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
                ctx.fillStyle = '#fff';
                ctx.fill();
                ctx.closePath();
            }

            Snow.prototype.update = function() {
                this.y += this.speed;
                this.x += this.wind;

                if (this.y > ctx.canvas.height) {
                    this.y = 0;
                    this.x = random(0, width);
                }
            }

            function createSnow(count) {
                for (var i = 0; i < count; i++) {
                    snowflakes[i] = new Snow();
                }
            }

            function draw() {
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                for (var i = 0; i < snowflakes.length; i++) {
                    snowflakes[i].draw();
                }
            }

            function loop() {
                draw();
                update();
                animFrame(loop);
            }

            function random(min, max) {
                var rand = (min + Math.random() * (max - min)).toFixed(1);
                rand = Math.round(rand);
                return rand;
            }

            createSnow(150);
            loop();


            /*CALLBACK things */
            // loadOfferPage(offerParameter);
            // attachLinkMenu();
            // });
            // $('#homePage').load('home.html', function () {
            var strings ;
                $.getJSON(strings_file, function(json){
                    $.i18n.load(json);
                    strings=json;

            var typeText = function () {
                var theater = theaterJS({locale: 'pl', erase: false});
                theater.addActor('main', {speed: 0.5, accuracy: 1});
                theater.addActor('subText', {speed: 0.9, accuracy: 1});

                    strings.forEach(function (text) {
                        theater.addScene('main:' + text.main);
                        text.substrings.forEach(function (substring) {
                            theater.addScene('subText:' + substring);
                            theater.addScene(500);

                        });
                    });
                    theater
                        .addScene(theater.replay);
                };
                typeText(); // invoke

            });




        });
        $.get("what.html", function (data) {
            data = '"' + data + '"';
            $("#whatPage").html(data);
            var newHTML = $('#whatPage').html();
            $('#whatPage').html(newHTML.substr(1, newHTML.length - 2));
            $('#whatPage > div > div > div.section-text > h1')._t('offerSection.title');
            $('#whatPage > div > div > div.section-text > p')._t('offerSection.description');
            $('#uxSquare > a')._t('offerSection.uxTile');
            $('#offerSquare > a')._t('offerSection.consultationsTile');
            $('#researchSquare > a')._t('offerSection.userssurveyTile');
            $('#webdevSquare > a')._t('offerSection.webTile');
            // $('#whatPage').load('what.html', function () {
            //         $('#uxSquare').click(function(){
            //             // showOfferPage("ux");
            //             window.location.href='offer'
            //         })
            //     $('#offerSquare').click(function(){
            //         // showOfferPage("consulting");
            //         // window.location.href='offer'
            //     })
            //     $('#researchSquare').click(function(){
            //         // showOfferPage("research");
            //         window.location.href='offer'
            //     })
            //     $('#webdevSquare').click(function(){
            //         // showOfferPage("webdev");
            //         window.location.href='offer'
            //     })
            // $('.offer-square').each(function(i,offersquare){
            //     $(offersquare).css('opacity','0');
            //     var svg = Snap($(offersquare).find('svg')[0]);
            //     var yourElement = svg.selectAll('path').forEach(function(path,i){
            //         var len = path.getTotalLength();
            //
            //             path.attr({
            //                 stroke: 'black',
            //                 fill: 'none',
            //                 "stroke-dasharray": len + " " + len,
            //                 "stroke-dashoffset": len
            //             })
            //     })
            // })
            // $('.offer-cell').click(function(){
            //     showOfferPage()
            // })

            $.get("how.html", function (data) {
                data = '"' + data + '"';
                $("#howPage").html(data);
                var newHTML = $('#howPage').html();
                $('#howPage').html(newHTML.substr(1, newHTML.length - 2));
                $('#howPage > div.page-content > div > div.section-text > h1')._t('workingSection.title');
                $('#howPage > div.page-content > div > div.section-text > div > p')._t('workingSection.description');
                $('#tspan5101')._t('workingSection.icon1text');
                $('#tspan4940')._t('workingSection.icon1text');
                $('#tspan4960')._t('workingSection.icon2text');
                $('#tspan5156')._t('workingSection.icon2text');
                $('#tspan5160')._t('workingSection.icon3text');
                $('#tspan4982')._t('workingSection.icon3text');
                $('#tspan5164')._t('workingSection.icon4text');
                $('#tspan4998')._t('workingSection.icon4text');
                $('#tspan5168')._t('workingSection.icon5text');
                $('#text5012')._t('workingSection.icon5text');
                $('#tspan5172')._t('workingSection.icon6text');
                $('#tspan5038')._t('workingSection.icon6text');
                $('#tspan3408')._t('workingSection.endtext');
                $('#seeFullProcess')._t('workingSection.lookprocessButton');
                // $('#howPage').load('how.html', function () {
                howSvg = Snap($('.process-container').find('svg')[0]);
                var homepageProcessPaths = howSvg.selectAll('#foreground path').items;
                howSvg.selectAll('#processIcon1 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon1 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon2 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon2 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon3 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon3 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon4 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon4 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon5 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon5 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon6 path').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processIcon6 text').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#processLastText').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                howSvg.selectAll('#showFullProcessText').items.reverse().forEach(function (path, i) {
                    path.attr({opacity: 0})
                })
                homepageProcessPaths.forEach(function (path, i) {
                    var len = path.getTotalLength();
                    path.attr({
                        // "stroke-width": 2,
                        "stroke-dasharray": len + " " + len,
                        "stroke-dashoffset": len
                    })
                })
                $.get("projects.html", function (data) {
                    data = '"' + data + '"';
                    $("#projectsPage").html(data);
                    var newHTML = $('#projectsPage').html();
                   $('#projectsPage').html(newHTML.substr(1, newHTML.length - 2));
                   $('#projectsPage > div > div > div.section-text > h1')._t('projectSection.title');
                   $('#projectsPage > div > div > div.section-text > p')._t('projectSection.description');
                   $('div.carousel-slide.fastest > div.carousel-desc > p > strong')._t('Carousel.Projects.Fastest.sections1');
                   $('div.carousel-slide.fastest > div.carousel-desc > p:eq(1) ')._t('Carousel.Projects.Fastest.sections2');
                   $('div.carousel-slide.fastest > div.carousel-slide-bullets > p ')._t('Carousel.Projects.Fastest.sections3');
                   $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(0)')._t('Carousel.Projects.Fastest.ullist1');
                   $('.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(1)')._t('Carousel.Projects.Fastest.ullist2');
                   $('.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(2)')._t('Carousel.Projects.Fastest.ullist3');
                   $('.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(3)')._t('Carousel.Projects.Fastest.ullist4');
                   $('.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(4)')._t('Carousel.Projects.Fastest.ullist5');


                    $('div.carousel-slide.aiut > div.carousel-desc > p > strong')._t('Carousel.Projects.Aiut.sections1');
                    $('div.carousel-slide.aiut > div.carousel-desc > p:eq(1)')._t('Carousel.Projects.Aiut.sections2');
                    $('div.carousel-slide.aiut > div.carousel-slide-bullets > p')._t('Carousel.Projects.Aiut.sections3');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(0)')._t('Carousel.Projects.Aiut.ullist1');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(1)')._t('Carousel.Projects.Aiut.ullist2');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(2)')._t('Carousel.Projects.Aiut.ullist3');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(3)')._t('Carousel.Projects.Aiut.ullist4');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(4)')._t('Carousel.Projects.Aiut.ullist5');

                    $('div.carousel-slide.infotower > div.carousel-desc > p > strong')._t('Carousel.Projects.Infotower.sections1');
                    $('div.carousel-slide.infotower > div.carousel-desc > p:eq(1)')._t('Carousel.Projects.Infotower.sections2');
                    $('div.carousel-slide.infotower > div.carousel-slide-bullets > p ')._t('Carousel.Projects.Infotower.sections3');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(0)')._t('Carousel.Projects.Infotower.ulist1');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(1)')._t('Carousel.Projects.Infotower.ulist2');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(2)')._t('Carousel.Projects.Infotower.ulist3');
                    $('div.carousel-slide.fastest > div.carousel-slide-bullets > ul > li:eq(3)')._t('Carousel.Projects.Infotower.ulist4');


                    // $('#projectsPage').load('projects.html', function (){
                    $.get("contact.html", function (data) {
                        data = '"' + data + '"';
                        $("#contactPage").html(data);
                        var newHTML = $('#contactPage').html();
                        $('#contactPage').html(newHTML.substr(1, newHTML.length - 2));
                        $('#contactPage > div > div > div.text-button-container > h1')._t('contactSection.headtitle');
                        $('#contactPage > div > div > div.text-button-container > h4')._t('contactSection.banner');
                        $('#contactPage > div > div > div.text-button-container > button')._t('contactSection.button');
                        $('#contactPage > div > div > div.contact-footer > div.footer-cell.footer-contact-us > h5')._t('contactSection.contactTitle1');
                        $('#contactPage > div > div > div.contact-footer > div.footer-cell.footer-coffee > h5')._t('contactSection.contactTitle2');
                        $('#contactPage > div > div > div.contact-footer > div.footer-cell.footer-coffee > div > span > a')._t('contactSection.howarrivelink');
                        // $('#contactPage').load('contact.html', function () {
                        if (detectIE()) {
                            drawHomepageProcess()
                        }
                        /**
                         * Load HomePage Dynamic Content
                         */
                        if ($(window).width() >= 900 && detectIE() === false) {
                            initializeHomepageScrollMagic()
                        } else {
                            drawHomepageProcess()
                        }

                        attachHomePageMenu()

                        var href = window.location.href;
                        var subPage = href.substr(href.lastIndexOf('/') + 1);
                        if (subPage.length > 0) {
                            isExternalCall = true
                        }

                        $("section.pagePanel").each(function (scrollCardId, scrollCard) {
                            if (scrollCard.id === subPage) {
                                homepageScrollMagicController.scrollTo(scrollCard);
                            }
                        });


                        $(".offer-square").mouseenter(function () {
                            var svg = Snap($(this).find('svg')[0]);
                            var yourElement = svg.selectAll('path').forEach(function (path, i) {
                                var len = path.getTotalLength();
                                var lineAnimation = function () {
                                    path.attr({
                                        stroke: '#d9534f',
                                        fill: '#d9534f',
                                        "stroke-dasharray": len + " " + len,
                                        "stroke-dashoffset": len
                                    }).animate({"stroke-dashoffset": 10}, 1, mina.easeinout, fillAnimation);
                                }
                                var fillAnimation = function () {
                                    if (path.attr('fill') === 'none') {
                                        path.attr({'fill': '#d9534f'})
                                    }
                                }
                                lineAnimation()
                            })
                        })
                        $(".offer-square").mouseleave(function () {
                            var svg = Snap($(this).find('svg')[0]);
                            svg.selectAll('path').items.reverse().forEach(function (path, i) {
                                var len = path.getTotalLength();
                                path.attr({
                                    stroke: 'black',
                                    fill: 'black',
                                    "stroke-dasharray": len + " " + len,
                                    "stroke-dashoffset": len
                                }).animate({"stroke-dashoffset": 10}, 1, mina.easeinout, function () {
                                    path.attr({'fill': 'black'})
                                });
                            })
                        })

                        if (detectIE()) {
                            $(".projects-carousel").slick({
                                // infinite: true,
                                // slidesToShow: 1,
                                // useCSS:false,
                                // autoplay: true,
                                // // slidesToScroll: 1,
                                // arrows: true,
                                responsive: [
                                    {
                                        breakpoint: 1100,
                                        settings: {
                                            slidesToShow: 1
                                        }
                                    },
                                    {
                                        breakpoint: 900,
                                        settings: "unslick"
                                    }
                                ]
                            });
                        } else {
                            $(".projects-carousel").slick({
                                // infinite: true,
                                slidesToShow: 2,
                                // useCSS:false,
                                // autoplay: true,
                                // // slidesToScroll: 1,
                                // arrows: true,
                                responsive: [
                                    {
                                        breakpoint: 1100,
                                        settings: {
                                            slidesToShow: 1
                                        }
                                    },
                                    {
                                        breakpoint: 900,
                                        settings: "unslick"
                                    }
                                ]
                            });
                        }

                        // resizeFonts()
                        $('.homepageBounce').click(function () {
                                homepageScrollMagicController.scrollTo($('#what')[0])
                            }
                        )
                        $('.whatBounce').click(function () {
                                homepageScrollMagicController.scrollTo($('#how')[0])
                            }
                        )
                        $('.howBounce').click(function () {
                                homepageScrollMagicController.scrollTo($('#projects')[0])
                            }
                        )
                        $('.projectsBounce').click(function () {
                                homepageScrollMagicController.scrollTo($('#contact')[0])
                            }
                        )
                    });
                });
            });
        });

    };
    var changePathColor = function (path, color) {
        return path.attr({
            stroke: color,
            fill: color
        })
    }
    var preparePath = function (path) {
        var len = path.getTotalLength();
        return path.attr({
            stroke: 'black',
            fill: 'none',
            // "stroke-width": 2,
            "stroke-dasharray": len + 1 + " " + len + 1,
            "stroke-dashoffset": len + 1
        })
    }

    var loadOfferPage = function (offerParameter) {
        $('#contactFooter').load('contact.html', function () {
            //    $('.offer-categories ul li').each(function(index,element){
            //        $(this).click(function(){
            //            $('.categories-content').hide().eq(index).show();
            //        })
            //    })
            //     // $('.categories-content-section p').hide();
            //     $('.categories-content-section').mouseenter(function(){
            //        $(this).find('p').css('opacity',1);
            //
            //
            //     });
            //     $('.categories-content-section').mouseleave(function(){
            //         $(this).find('p').css('opacity',0.05);
            //
            //     });
            //     // if(offerParameter){
            //     //     if(offerParameter === 'ux') {
            //     //         ux();
            //     //     }else if(offerParameter === 'consulting') {
            //     //         consulting();
            //     //     }else if(offerParameter === 'research'){
            //     //         research();
            //     //     }else if(offerParameter === 'webdev'){
            //     //         webdev();
            //     //     }
            //     //
            //     // }else{
            //     //     ux();
            //     // }
            //
            //
            //     // $('.categories-content-section p').hide();
            //     $('.categories-content').hide().eq(0).show();


            /* OLD VERSION : */
            $('#offerFooter').load('contact.html', function () {
                $('.left-carousel').slick({
                    // dots: true,
                    arrows: false,
                    // fade: true,
                    // infinite: true,
                    // slidesToShow: 1,
                    // slidesToScroll: 1,
                    // centerMode: true,
                    variableWidth: true
                });
                $('.right-carousel').each(function (i) {
                    $(this).slick({
                        // infinite: true,
                        // slidesToShow: 1,
                        // slidesToScroll: 1
                        dots: true,
                        variableWidth: true,
                        autoplay: true
                    });
                })


                $('.left-carousel').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                    $('.right-carousel').hide();
                    $('.right-carousel').eq(nextSlide).show();
                    $('.offer-categories h3').each(function () {
                        $(this).removeClass('offerCategoryActive')
                    })
                    $('.offer-categories h3').eq(nextSlide).addClass('offerCategoryActive')
                });
                var ux = function () {
                    $('.left-carousel').slick('slickGoTo', 0, true);
                    $('.offer-categories h3').each(function () {
                        $(this).removeClass('offerCategoryActive')
                    })
                    $(this).addClass('offerCategoryActive')
                    $('.offer-categories h3').eq(0).addClass('offerCategoryActive')
                }
                var consulting = function () {
                    $('.left-carousel').slick('slickGoTo', 1, true);
                    $('.offer-categories h3').each(function () {
                        $(this).removeClass('offerCategoryActive')
                    })
                    $(this).addClass('offerCategoryActive')
                    $('.offer-categories h3').eq(1).addClass('offerCategoryActive')
                }
                var research = function () {
                    $('.left-carousel').slick('slickGoTo', 2, true);
                    $('.offer-categories h3').each(function () {
                        $(this).removeClass('offerCategoryActive')
                    })
                    $(this).addClass('offerCategoryActive')
                    $('.offer-categories h3').eq(2).addClass('offerCategoryActive')
                }
                var webdev = function () {
                    $('.left-carousel').slick('slickGoTo', 3, true);
                    $('.offer-categories h3').each(function () {
                        $(this).removeClass('offerCategoryActive')
                    })
                    $(this).addClass('offerCategoryActive')
                    $('.offer-categories h3').eq(3).addClass('offerCategoryActive')
                }
                $('#offerUx').click(function () {
                    ux();
                })
                $('#offerConsulting').click(function () {
                    consulting();
                })
                $('#offerResearch').click(function () {
                    research();
                })
                $('#offerWebdev').click(function () {
                    webdev();
                })
                $('.right-carousel').hide();

                $('.left-carousel').slick('slickGoTo', 0, true);
                setTimeout(function () {
                    $('.offer-overlay').hide()
                }, 300);
                if (offerParameter) {
                    if (offerParameter === 'ux') {
                        ux();
                    } else if (offerParameter === 'consulting') {
                        consulting();
                    } else if (offerParameter === 'research') {
                        research();
                    } else if (offerParameter === 'webdev') {
                        webdev();
                    }

                } else {
                    ux();
                }
               })
        })
    }

    var loadContactPage = function () {
        $('#contactFooter').load('contact.html', function () {

        })
        $('#contactSubpage').load('form.html', function () {
            $('.selectpicker').selectpicker();
            $('#submitFormButton').click(function () {
                var mailData = {
                    name: $('#nameInput').val(),
                    topic: $('#talkAboutSelect option:selected').text(),
                    email: $('#emailInput').val(),
                    message: $('#messageInput').val(),
                    phoneNumber: $('#phoneNumberInput').val(),
                }
                $('#submitFormButton').css('opacity', '0.5');
                $('#submitFormButton').html('Wysyłanie...');
                $.post("https://mailer-pbtns.herokuapp.com/pbtnsform", mailData, function () {
                    $('#submitFormButton').css('background-color', 'green');
                    $('#submitFormButton').css('border-color', 'green');
                    $('#submitFormButton').html('Dziękujemy!');
                    setTimeout(function () {
                        window.location.href = 'http://www.pushbuttons.pl'
                    }, 1500)
                })
            })
            // alert("contact loaded")
        })
    }
    var loadProcessPage = function () {
        // $('#processPage').load('process.html', function () {
        // $('.process-svg-container svg').removeAttr('viewBox');
        // $('.process-svg-container svg').each(function () {
        //
        //     $(this)[0].setAttribute('viewBox', '0 0 '+window.innerHeight+' '+window.innerHeight);
        // });
        var svg = Snap($('#processPage').find('svg')[0]);
        // $("#processPage").mouseenter(function(){
        //     clearTimeout($(this).data('timeoutId'));
        //     mouseInSvg = true
        //     console.log("MOUSE IN SVG")
        // }).mouseleave(function(){
        //     var someElement = $(this),
        //         timeoutId = setTimeout(function(){
        //             mouseInSvg = false;
        //             console.log("MOUSE OUT SVG")
        //         }, 650);
        //     //set the timeoutId, allowing us to clear this trigger if the mouse comes back over
        //     someElement.data('timeoutId', timeoutId);
        // });


        var pathProcessed = 0;
        var paths = svg.selectAll('#foreground path').items;
        paths.push = svg.selectAll('circle').items;
        var texts = svg.selectAll('text').items

        texts.forEach(function (text, i) {
            text.attr({'opacity': '0'});
        })
        // svg.select('#startText').attr({'opacity':'1'});
        var backgroundPaths = svg.selectAll('#background path').items;
        var speed = 400;

        paths.forEach(function (path, i) {
            backgroundPaths.forEach(function (path, i) {
                changePathColor(path, '#adadad')
            })
            preparePath(path)
            pathProcessed++
            if (pathProcessed === paths.length) {
                // init homepageScrollMagicController
                var processController = new ScrollMagic.Controller({
                    // container: '.processSvgContainer'
                    globalSceneOptions: {
                        triggerHook: "0.15",
                        reverse: true
                    }

                });
                processController.scrollTo(scrollTo);

                var now = new Date().getTime();

                var currentScene = {};

                var firstProcess = function () {
                    currentScene = this;
                    $(".starting-arrow").css('display', 'none').promise().done(function () {
                        svg.select('#process1line1').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                            svg.select('#process1line2a').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                svg.select('#process1line2aa').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                });
                            });
                            svg.select('#process1line2b').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                svg.select('#process1line2bb').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                    svg.selectAll('#process1icon1a path').items.reverse().forEach(function (path, i) {
                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                            path.attr({'fill': 'black'})
                                        });
                                        if (i === svg.selectAll('#process1icon1a path').items.length - 1) {
                                            svg.select('#process1text1').attr('opacity', '1')
                                            svg.selectAll('#process1icon1b path').items.forEach(function (path, i) {
                                                path.animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                                    path.attr({'fill': 'black'})
                                                });
                                                if (i === svg.selectAll('#process1icon1b path').items.length - 1) {
                                                    svg.select('#process1text2').attr('opacity', '1')
                                                    svg.select('#process1line3a').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                                        svg.select('#process1line4a').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {

                                                        })
                                                    })
                                                    svg.select('#process1line3b').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                                        svg.select('#process1line4b').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {
                                                            svg.select('#process1line5').animate({"stroke-dashoffset": 0}, speed, mina.easein, function () {

                                                            })
                                                        })
                                                    })
                                                }
                                            })
                                        }
                                    })
                                });

                            });
                        });
                    });
                }

                var secondProcess = function () {
                    currentScene = this;
                    // var process1Paths = [,,svg.select('#process1line3')]
                    svg.select('#process2line1').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                        svg.selectAll('#process2icon1 path').items.reverse().forEach(function (path, i) {
                            path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                path.attr({'fill': 'black'})
                            });
                            if (i === svg.selectAll('#process2icon1 path').items.length - 1) {
                                svg.select('#process2text1').attr('opacity', '1')
                                svg.select('#process2line2').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                    svg.selectAll('#process2icon2 path').items.reverse().forEach(function (path, i) {
                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                            path.attr({'fill': 'black'})
                                        });
                                        if (i === svg.selectAll('#process2icon2 path').items.length - 1) {
                                            svg.select('#process2text2').attr('opacity', '1')
                                            svg.select('#process2line3').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                svg.selectAll('#process2icon3 path').items.reverse().forEach(function (path, i) {
                                                    path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                        path.attr({'fill': 'black'})
                                                    });
                                                    if (i === svg.selectAll('#process2icon3 path').items.length - 1) {
                                                        svg.select('#process2text3').attr('opacity', '1')
                                                        svg.select('#process2line4').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {

                                                        })
                                                    }
                                                })

                                            })
                                        }
                                    })
                                });
                            }
                        })


                    });
                }

                var thirdProcess = function () {
                    currentScene = this;
                    // var process1Paths = [,,svg.select('#process1line3')]
                    svg.select('#process3line1').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                        svg.selectAll('#process3icon1 path').items.reverse().forEach(function (path, i) {
                            path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                path.attr({'fill': 'black'})
                            });
                            if (i === svg.selectAll('#process3icon1 path').items.length - 1) {
                                svg.select('#process3text1').attr('opacity', '1')
                                svg.select('#process3line2').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                    svg.selectAll('#process3icon2 path').items.reverse().forEach(function (path, i) {
                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                            path.attr({'fill': 'black'})
                                        });
                                        if (i === svg.selectAll('#process3icon2 path').items.length - 1) {
                                            svg.select('#process3text2').attr('opacity', '1')
                                            svg.select('#process3line3').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {

                                            })
                                        }
                                    })
                                })
                            }
                        })
                    });
                };

                var fourthProcess = function () {
                    currentScene = this;
                    // var process1Paths = [,,svg.select('#process1line3')]
                    svg.select('#process4line1').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                        svg.select('#process4line2').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                            svg.selectAll('#process4icon1 path').items.reverse().forEach(function (path, i) {
                                path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                    path.attr({'fill': 'black'})
                                });
                                if (i === svg.selectAll('#process4icon1 path').items.length - 1) {
                                    svg.select('#process4text1').attr('opacity', '1')
                                    svg.select('#process4line3').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                        svg.selectAll('#process4icon2 path').items.reverse().forEach(function (path, i) {
                                            path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                path.attr({'fill': 'black'})
                                            });
                                            if (i === svg.selectAll('#process4icon2 path').items.length - 1) {
                                                svg.select('#process4text2').attr('opacity', '1')
                                                svg.select('#process4line4').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                    svg.selectAll('#process4icon3 path').items.reverse().forEach(function (path, i) {
                                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                            path.attr({'fill': 'black'})
                                                        });
                                                        if (i === svg.selectAll('#process4icon3 path').items.length - 1) {
                                                            svg.select('#process4text3').attr('opacity', '1')
                                                            svg.select('#process4line5').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                                svg.select('#process4line6').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                                    // highlightIterationPath(svg)
                                                                })
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        })
                                    })
                                }
                            })
                        });
                    });
                }

                var fifthProcess = function () {
                    currentScene = this;
                    // var process1Paths = [,,svg.select('#process1line3')]
                    svg.select('#process5line1').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                        svg.selectAll('#process5icon1 path').items.reverse().forEach(function (path, i) {
                            path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                path.attr({'fill': 'black'})
                            });
                            if (i === svg.selectAll('#process5icon1 path').items.length - 1) {
                                svg.select('#process5text1').attr('opacity', '1')
                                svg.select('#process5line2').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                    svg.selectAll('#process5icon2 path').items.reverse().forEach(function (path, i) {
                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                            path.attr({'fill': 'black'})
                                        });
                                        if (i === svg.selectAll('#process5icon2 path').items.length - 1) {
                                            svg.select('#process5text2').attr('opacity', '1')
                                            svg.select('#process5line3').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                svg.select('#process5line2').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                    svg.selectAll('#process5icon3 path').items.reverse().forEach(function (path, i) {
                                                        path.animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {
                                                            path.attr({'fill': 'black'})
                                                        });
                                                        if (i === svg.selectAll('#process5icon3 path').items.length - 1) {
                                                            svg.select('#process5text3').attr('opacity', '1')
                                                            svg.select('#process5line4').animate({"stroke-dashoffset": 0}, speed, mina.easeinout, function () {

                                                            })
                                                        }
                                                    })
                                                })
                                            })
                                        }
                                    })
                                })
                            }
                        })
                    });
                };
                var processDrawingFunctions = [firstProcess, secondProcess, thirdProcess, fourthProcess, fifthProcess];
                var processSections = $('.process-section');

                $('.process-desc').each(function (i, processDescDiv) {
                    $(this).css('min-height', processSections[i].getBoundingClientRect().bottom -
                        processSections[i].getBoundingClientRect().top);
                    $(this).css("top", processSections[i].getBoundingClientRect().top)
                    if (i === $('.process-desc').length - 1) {
                        $('.process-description-container').css("opacity", "1")
                        $(".loader").hide();
                    }
                })
                // descriptions = $('.process-desc')

                $('#processFooter').load('contact.html', function () {
                    $.scrollify({
                        interstitialSection: '.interstatial-section',
                        section: ".process-section",
                        sectionName: false,
                        scrollbar: false,
                        offset: -$(window).innerHeight() / 4,
                        before: function (i, sections) {

                        },
                        after: function (i, sections) {
                            /*
                             *  set current desrcription state
                             */
                            if (i > 1) {
                                $('.next-section-action').eq(i - 2).hide();
                            }
                            if (i > 0) {
                                // $($('.process-desc').each(function(j,desc){
                                //     // if(j < i - 1){
                                //         // $(desc).addClass('blur#d9534f');
                                //         // $('.process-section').eq(j).addClass('blur#d9534f')
                                //         // $('.process-desc').removeClass('process-desc-active');
                                //     // }
                                //
                                // }))
                                // $('.process-section').eq([i-1]).removeClass('blur#d9534f')
                                // $($('.process-desc')[i-1]).removeClass('blur#d9534f')
                                $($('.process-desc')[i - 1]).addClass('process-desc-active')

                                processDrawingFunctions[i - 1]();
                            }
                            else {
                                // $('.starting-arrow').show();
                            }
                            $('#processFooter > div > div > div.text-button-container > h1')._t('contactSection.headtitle');
                            $('#processFooter > div > div > div.text-button-container > h4')._t('contactSection.banner');
                            $('#processFooter > div > div > div.text-button-container > button')._t('contactSection.button');
                            $('#processFooter > div > div > div.contact-footer > div.footer-cell.footer-contact-us > h5')._t('contactSection.contactTitle1');
                            $('#processFooter > div > div > div.contact-footer > div.footer-cell.footer-coffee > h5')._t('contactSection.contactTitle2');
                            $('#processFooter > div > div > div.contact-footer > div.footer-cell.footer-coffee > div > span > a')._t('contactSection.howarrivelink');
                        }
                    });
                    $('.next-section-action').click(function () {
                        $.scrollify.next();
                        $(this).hide();
                    })
                    // document.location = "";
                    setTimeout(function () {
                        scrollTo(0, -1);
                    }, 0);
                    $.scrollify.move(1);

                });
            }
            ;
        })


        // });
    }
    var initializeHomepageScrollMagic = function () {
        console.log("Initializing Magic scroll . . . ")
        scrollMagicInitialized = true
        homepageScrollMagicController = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: '0.1'
            }
        });
        homepageScrollMagicController.scrollPos(function () {
            if (window.innerWidth >= 800) {
                return window.pageYOffset;
            } else {
                return 0;
            }
        });
        // get all slides
        var slides = document.querySelectorAll(".scrollContainer section.pagePanel");

        new ScrollMagic.Scene({
            triggerElement: slides[0]
        })
            .setPin(slides[0])
            .setClassToggle(".menu-home", "menu-item-active")
            // .addIndicators() // add indicators (requires plugin)
            .addTo(homepageScrollMagicController).on("start", function (event) {
            $('.menu-item').removeClass("menu-item-active");
            linkHistoryApi(this.triggerElement().id);
            $('.menu-home').addClass("menu-item-active");

        }).on("leave", function (event) {
            $('.menu-item').removeClass("menu-item-active");
            linkHistoryApi(this.triggerElement().id);
            $('.menu-home').addClass("menu-item-active");
        })
        var scenes = []
        $.each(slides, function (i, slide) {
            if (i > 0) {
                var scene = new ScrollMagic.Scene({
                    triggerElement: slides[i]
                })
                    .setPin(slides[i])
                    .setClassToggle(".menu-" + slide.id, "menu-item-active")
                    // .addIndicators() // add indicators (requires plugin)
                    .addTo(homepageScrollMagicController).on("start", function (event) {
                        if (this.triggerElement().id === 'how') {
                            $('.menu').addClass('black');
                        } else {
                            $('.menu').removeClass('black');
                        }
                        $('.menu-item').removeClass("menu-item-active");
                        linkHistoryApi(this.triggerElement().id);

                        $('.menu-' + slide.id).addClass("menu-item-active");//slides[i].id
                    }).on("leave", function (event) {
                        // $('.menu').css('background-color',$('#'+slides[i-1].id).css('background-color'));
                        if (slides[i - 1].id === 'contact' || slides[i - 1].id === 'how') {
                            $('.menu').addClass('black');
                        } else {
                            $('.menu').removeClass('black');
                        }
                        $('.menu-item').removeClass("menu-item-active");
                        linkHistoryApi(slides[i - 1].id);//slides[i-1]
                        $('.menu-' + slides[i - 1].id).addClass("menu-item-active"); //slides[i-1].id

                    }).on("progress", function (event) {

                    });
                scenes.push(scene);
            }
        });

        scenes.forEach(function (scene) {


            if (scene.triggerElement().id === 'how') {
                scene.on("start", function (event) {
                    $('.menu-item').removeClass("menu-item-active");
                    $('.menu').addClass('black');
                    linkHistoryApi(this.triggerElement().id);
                    $('.menu-' + this.triggerElement().id).addClass("menu-item-active");//slides[i].id
                    if (processAnimationDone === false) {
                        processAnimationDone = true;
                        var homePageProcessSpeed = 1000;
                        drawHomepageProcess();
                    }

                })
            }
        })
        homepageScrollMagicController.scrollTo(scrollTo);
        /*
         CONTACT CAROUSEL
         */


    }


    var hamburgerVisible = false;
    $('#hamburger-menu-icon').click(function () {
        if ($(".menu-bar").is(":hidden")) {
            hamburgerVisible = true;
            $(".pbtns-logo").slideUp()
            $(".menu-bar").slideDown(500, function () {
            });


        } else {
            hamburgerVisible = false;
            // $( ".bw-logo-container" ).show();

            $(".menu-bar").slideUp(500, function () {

            });

            $(".pbtns-logo").slideDown(function () {
                $(this).css('display', 'flex')
            });


        }

    });
    var disableScrollMagic = function () {
        if (homepageScrollMagicController !== 'undefined' && homepageScrollMagicController !== null) {
            homepageScrollMagicController.destroy(true);
        }
        // $('')
        // homepageScrollMagicController = null;
        scrollMagicInitialized = false
    }
    /**
     * FrontendRouting - niszczyc inne kontenery docelowo, bo sie zacznie mieszac to co pod spodem itp.
     */

    var drawHomepageProcess = function () {
        var homePageProcessSpeed = 750;
        if (detectIE()) {
            homePageProcessSpeed = 1;
        }
        howSvg.selectAll('#processIcon1 path').items.reverse().forEach(function (path, i) {
            path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                path.attr({'opacity': 1})
            });
            if (i === howSvg.selectAll('#processIcon1 path').items.length - 1) {
                howSvg.select('#processIcon1 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                howSvg.select('#processLine1').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {
                    howSvg.selectAll('#processIcon2 path').items.reverse().forEach(function (path, i) {
                        path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                            path.attr({'opacity': 1})
                        });
                        if (i === howSvg.selectAll('#processIcon2 path').items.length - 1) {
                            howSvg.select('#processIcon2 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                            howSvg.select('#processLine2').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {

                                howSvg.selectAll('#processIcon3 path').items.reverse().forEach(function (path, i) {
                                    path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                                        path.attr({'opacity': 1})
                                    });
                                    if (i === howSvg.selectAll('#processIcon3 path').items.length - 1) {
                                        howSvg.select('#processIcon3 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                                        howSvg.select('#processLine3').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {
                                            howSvg.selectAll('#processIcon4 path').items.reverse().forEach(function (path, i) {
                                                path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                                                    path.attr({'opacity': 1})
                                                });
                                                if (i === howSvg.selectAll('#processIcon4 path').items.length - 1) {
                                                    howSvg.select('#processIcon4 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                                                    howSvg.select('#processLine4').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {
                                                        howSvg.selectAll('#processIcon5 path').items.reverse().forEach(function (path, i) {
                                                            path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                                                                path.attr({'opacity': 1})
                                                            });
                                                            if (i === howSvg.selectAll('#processIcon5 path').items.length - 1) {
                                                                howSvg.select('#processIcon5 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                                                                howSvg.select('#processLine5').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {
                                                                    howSvg.selectAll('#processIcon6 path').items.reverse().forEach(function (path, i) {
                                                                        path.animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {
                                                                            path.attr({'opacity': 1})
                                                                        });
                                                                        if (i === howSvg.selectAll('#processIcon6 path').items.length - 1) {
                                                                            howSvg.select('#processIcon6 text').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout);
                                                                            howSvg.select('#processLine6').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easeinout, function () {

                                                                                howSvg.select('#processLastText').animate({"opacity": 1}, homePageProcessSpeed, mina.easeinout, function () {

                                                                                    howSvg.select('#showFullProcessRect').animate({"stroke-dashoffset": 0}, homePageProcessSpeed, mina.easein, function () {
                                                                                        howSvg.select('#showFullProcessText').animate({"opacity": 1}, homePageProcessSpeed, mina.easein, function () {
                                                                                            if (!detectIE()) {
                                                                                                howSvg.select('#showFullProcessText').hover(
                                                                                                    function () {
                                                                                                        // this.animate({'stroke':'#d9534f'},homePageProcessSpeed,mina.easin)
                                                                                                        this.attr({'fill': '#d9534f'})
                                                                                                        howSvg.select('#showFullProcessRect').attr({'fill': '#d9534f'});
                                                                                                        // howSvg.select('#showFullProcessRect').animate({'stroke':'#d9534f'},homePageProcessSpeed,mina.easin)
                                                                                                    },
                                                                                                    function () {
                                                                                                        this.animate({'stroke': 'white'}, homePageProcessSpeed, mina.easin)
                                                                                                        this.attr({'fill': 'white'})
                                                                                                        howSvg.select('#showFullProcessRect').attr({'fill': 'none'})
                                                                                                        // howSvg.select('#showFullProcessRect').animate({'stroke':'white'},homePageProcessSpeed,mina.easin)
                                                                                                    })
                                                                                            }

                                                                                            // howSvg.selectAll('.hoverable').forEach(function(hoverable){
                                                                                            //     hoverable.hover( function(){
                                                                                            //             this.animate({'stroke':'#d9534f'},homePageProcessSpeed,mina.easin)
                                                                                            //             // this.attr({'fill':'#d9534f'})
                                                                                            //                         if( howSvg.selectAll('.hoverable path').length > 0) {
                                                                                            //                             howSvg.selectAll('.hoverable path').forEach(function(path){
                                                                                            //                                 this.animate({'stroke':'#d9534f'},homePageProcessSpeed,mina.easin)
                                                                                            //                                 this.attr({'fill':'#d9534f'})
                                                                                            //                             })
                                                                                            //                         }
                                                                                            //         },
                                                                                            //         function(){
                                                                                            //             this.animate({'stroke':'white'},homePageProcessSpeed,mina.easin)
                                                                                            //             // this.attr({'fill':'white'})
                                                                                            //             if( howSvg.selectAll('.hoverable path').length > 0) {
                                                                                            //                 howSvg.selectAll('.hoverable path').forEach(function(path){
                                                                                            //                     this.animate({'stroke':'white'},homePageProcessSpeed,mina.easin)
                                                                                            //                     // this.attr({'fill':'white'})
                                                                                            //                 })
                                                                                            //             }
                                                                                            //
                                                                                            //         })
                                                                                            //
                                                                                            // })
                                                                                            $('#showFullProcess').css('cursor', 'pointer');
                                                                                            $('#showFullProcessText').click(function () {
                                                                                                document.location.href = '/process'
                                                                                            })

                                                                                        })
                                                                                    })
                                                                                });

                                                                            })
                                                                        }
                                                                    })
                                                                })
                                                            }
                                                        })
                                                    })
                                                }
                                            })
                                        })
                                    }
                                })
                            })
                        }
                    })
                })
            }
        })

    }

    $(document).ready(function () {
        $.ajaxSetup({cache: false});
        $.cookiepolicy();

       var content_file = "";
       var lan = "";
        if((window.navigator.userLanguage || window.navigator.language)!='pl') {
            content_file = "en_content.json";
            strings_file = "en_strings.json"
            lan = "en";
        }
        else {
            content_file = "pl_content.json";
            strings_file = "pl_strings.json"
            lan = "pl";
        }

        var globalJsonVar;
        $.getJSON(content_file, function(json){
            $.i18n.load(json);
            $('#menu-what.menu-what.menu-item a')._t('menu-bar.text1');
            $('#menu-how.menu-how.menu-item a')._t('menu-bar.text2');
            $('#menu-projects.menu-projects.menu-item a')._t('menu-bar.text3');
            $('#menu-contact.menu-contact.menu-item a')._t('menu-bar.text4');
            $('.btn.btn-danger.btn-lg.menu-project-button')._t('homepage.other.buttonProject');
            $('.cookiepolicy span')._t('homepage.other.cookies');
            $('.cookiepolicy a')._t('homepage.other.closecookiesbutton');

            //$('#subText.typedSubtext')._t('main1.substring1');
            globalJsonVar=json;
        });
   $('#menu-what.menu-what.menu-item a')._t('offerSection.title');
        $(".navbar-toggle").on("click", function () {
            $(this).toggleClass("active");
        });
        if (document.URL.includes("process")) {
            showProcessPage();
        } else if (document.URL.endsWith('contactform')) {
            loadContactForm();
            window.document.title = "Pushbuttons.pl | Kontakt"
        } else if (document.URL.includes('offer')) {
            showOfferPage();
        } else {
            window.history.replaceState("", document.title, "");
            $('main').empty()
            $('main').load('homepage.html', function () {
                loadHomepage();
            })
        }

    });
    /**
     * Uwzglednic routing
     */

    $(window).resize(function (event) {

        // resizeFonts();

        if (window.innerWidth >= 900) {
            if ($("#pinContainer").length && scrollMagicInitialized === false && detectIE() === false) {
                initializeHomepageScrollMagic();
            }
            $('.menu-bar').css('display', 'flex')
            $('.pbtns-logo').show()
            // $( ".pbtns-logo-bw" ).css('display','none');
            // $(".bw-logo-container").css('display','none');

        } else {
            if ($("#pinContainer").length) {
                drawHomepageProcess();
                disableScrollMagic()
            }

            if (hamburgerVisible === true) {
                $('.menu-bar').css('display', 'flex')
                $('.pbtns-logo').css('display', 'none');
            } else {
                $('.menu-bar').css('display', 'none');
                $('.pbtns-logo').css('display', 'block');
            }
        }
    });
    if (detectIE()) {
        $('.pbtns-logo').click(function () {
            document.location.href = '/'
        })
    }


})();