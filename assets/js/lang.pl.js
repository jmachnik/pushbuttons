/**
 * Created by jakubmachnik on 12.12.2016.
 */
home = {
    typed: [
        {
            main: "Badamy",
            substrings: [
                "użyteczność rozwiązań",
                "potrzeby użytkowników",
                ""
            ]

        },
        {
            main: "Projektujemy",
            substrings: [
                "pozytywne doświadczenia",
                "strony www i aplikacje mobilne",
                ""
            ]

        },
        {
            main: "Doradzamy",
            substrings: [
                "jak połączyć design z technologią",
                "jak poprawić użyteczność rozwiązań",
                ""
            ]
        }
    ]
}

offer = {
    header: "Nasza oferta",
    description: "Każdy projekt to inne potrzeby, możliwości i cele. Dlatego zawsze indywidualnie dopasowujemy ofertę do klienta i projektu." +
    " Najważniejszym obszarem naszych działań jest user experience, ale możemy rozszerzyć nasze prace o prace programistyczne, graficzne " +
    "i marketing.",
    uxIcon: "projektowanie UX",
    consultIcon: "konsultacje",
    researchIcon: "badania z użytkownikami",
    webdevIcon: "web development"
}

how = {
    header: "jak pracujemy",
    description: "W Push Buttons kochamy przemyślane rozwiązania i dobry design. Rozumiemy użytkowników i technologię. W swojej pracy " +
    "łączymy user experience ze zrozumieniem i znajomością zagadnień technicznych. Potrafimy słuchać, zrozumieć i zaproponować najlepsze" +
    " rozwiązanie, które z pasją wdrożymy w życie.",
}

projects = {
    header: "nasze projekty",
    description: "Lubimy nowe wyzwania. Podejmujemy się badania i projektowania aplikacji z różnych obszarów merytorycznych. Dzięki sumiennemu " +
    "podejściu do etapu analizy możemy ze spokojem realizować nawet najbardziej wymagające projekty"
}

contact = {
    header: "UWIELBIAMY NOWE WYZWANIA",
    subheader: "Świetnie byłoby pracować z Tobą <p> nad Twoim nowym pomysłem",
    contactCellHeader: "Skontaktuj się z nami",
    coffeeCellHeader: "Wpadnij do nas na kawę"
}

